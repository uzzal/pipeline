<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/job-list/{team_id}', 'HomeController@jobList');

Route::get('/client', 'ClientController@index');
Route::post('/client', 'ClientController@store');
Route::get('/client/list/{id?}', 'ClientController@optionList');
Route::get('/client/{id}', 'ClientController@show');

Route::get('/project', 'ProjectController@index');
Route::post('/project', 'ProjectController@store');
Route::get('/project/list/{id?}', 'ProjectController@optionList');
Route::get('/project/{client_id}/{id?}', 'ProjectController@optionList');
Route::get('/team/list/{id?}', 'TeamController@optionList');
Route::post('/job', 'JobController@store');

if(false){
    Event::listen(\Illuminate\Database\Events\QueryExecuted::class, function($query)
    {
        echo '<code>'.$query->sql.'</code><br>';
    });
}