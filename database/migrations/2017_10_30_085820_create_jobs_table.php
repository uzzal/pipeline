<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('job_id');
            $table->integer('client_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('team_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->string('name', 200);
            $table->date('start_date');
            $table->date('end_date');
            $table->string('status', 50);
            $table->boolean('is_active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
