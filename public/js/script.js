$(function(){
    $('.datepicker').datetimepicker({
        format:'YYYY-MM-DD',
        allowInputToggle:true
    });

    JobStatus.init();
    JobList.init();
    Modals.init();
});

var JobList={
    init:function(){
        this.events();
    },
    events:function(){
        var that = this;
        $(document).on('change','#ddMembers', function(e){
            e.preventDefault();
            that.load(this.value);
        });
    },
    load:function(id){
        if(!id){
            id = 0;
        }
        $.ajax({
            'url':'/home/job-list/'+id,
            success:function(resp){
                $('#clientList').html(resp);
            }
        })
    }
};

var JobStatus={
    domJobStatus:null,
    init:function(){
        var that = this;
        this.domJobStatus = $('#domJobStatus');
        $(document).on('click','.btnJobStatus',function(e){
            e.preventDefault();
            $('.btnJobStatus').removeClass('btn-primary active').addClass('btn-default');
            $(this).removeClass('btn-default').addClass('btn-primary active');
            that.domJobStatus.val(($(this).text()));
        });
    }
};

var ClientRepo={
    domId:'.ddClient',
    load:function(){
        var that = this;
        $.ajax('/client/list',{
            success:function (resp) {
                $(that.domId).html(resp);
            }
        })
    },
    create:function(dom){
        var that = this;
        $('#frmClient').ajaxSubmit({
            resetForm: true,
            success:function(resp){
                $(that.domId).html(resp);
                alert('Client created successfully');
                dom.modal('hide');
            }
        });
    }
};

var ProjectRepo={
    domId:'#ddProject1',
    get:function(clientId, id){
        var that = this;
        if(!clientId){
            $(that.domId).html('<option value="">Select a Project</option>');
            return;
        }
        $.ajax('/project/'+clientId+'/'+id,{
            success:function (resp) {
                $(that.domId).html(resp);
            }
        })
    },
    create:function(dom){
        var that = this;
        $('#frmProject').ajaxSubmit({
            resetForm: true,
            success:function(resp){
                //$(that.domId).html(resp);
                alert('Project created successfully');
                dom.modal('hide');
            }
        });
    }
};

var TeamRepo={
    domId:'.ddTeam',
    load:function(){
        var that = this;
        $.ajax('/team/list',{
            success:function (resp) {
                $(that.domId).html(resp);
            }
        })
    }
};

var JobRepo={
    domId:'#client_',
    get:function(id){
        var that = this;
        $.ajax('/client/'+id,{
            success:function (resp) {
                $(that.domId+id).html(resp);
            }
        })
    },
    create:function(dom){
        var that = this;
        $('#frmJob').ajaxSubmit({
            resetForm: true,
            success:function(resp){
                if(resp.status=='success'){
                    that.get(resp.data.client_id);
                    alert('Job created successfully');
                    dom.modal('hide');
                }else{
                    alert('Sorry, Something went wrong!')
                }

            }
        });
    }
};

var Modals = {
    baseModal:null,
    clientModal:null,
    projectModal:null,
    init:function(){
        this.dom();
        this.events();
    },
    dom:function(){
        this.baseModal = $('#baseModal').modal({
            keyboard: false,
            backdrop: 'static',
            show:false
        }).on('show.bs.modal', function(e){
            ClientRepo.load();
            TeamRepo.load();
        });

        this.clientModal = $('#clientModal').modal({
            keyboard: false,
            backdrop: 'static',
            show:false
        });

        this.projectModal = $('#projectModal').modal({
            keyboard: false,
            backdrop: 'static',
            show:false
        });
    },
    events:function(){
        var that = this;
        $(document).on('click','#btnSaveBaseModal',function(e){
            e.preventDefault();
            JobRepo.create(that.baseModal);
        });

        $(document).on('click', '#btnSaveClientModal', function(e){
            e.preventDefault();
            ClientRepo.create(that.clientModal);
        });

        $(document).on('click', '#btnSaveProjectModal', function(e){
            e.preventDefault();
            ProjectRepo.create(that.projectModal);
        });

        $(document).on('change', '#ddClient1', function(e){
           e.preventDefault();
           ProjectRepo.get(this.value);
        });
    }
}