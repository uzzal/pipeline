<div class="modal fade" id="projectModal" tabindex="-1" role="dialog" aria-labelledby="projectModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="projectModal">Add New Project</h4>
            </div>
            <div class="modal-body">
                <!-- form start -->
                <form id="frmProject" method="post" action="{{url('project')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Client</label>
                        <select name="client_id" class="form-control ddClient">
                            <option value="">Select a client</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Project name</label>
                        <input name="name" type="text" class="form-control" placeholder="Project name">
                    </div>
                    {{-- Hard coded for now. No need to create table. We migh get it from
                    a prop file later on.--}}
                    <div class="form-group">
                        <label>Type of Project</label>
                        <select id="project_type" name="project_type" class="form-control">
                            <option value="Internal">Internal</option>
                            <option value="External">External</option>
                            <option value="JV">JV</option>
                        </select>
                    </div>


                </form>
                <!-- form end -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btnSaveProjectModal" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>