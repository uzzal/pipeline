<div class="modal fade" id="clientModal" tabindex="-1" role="dialog" aria-labelledby="clientModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="clientModal">Add New Client</h4>
            </div>
            <div class="modal-body">
                <!-- form start -->
                <form id="frmClient" method="post" action="{{url('client')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Client name</label>
                        <input type="text" name="name" class="form-control" placeholder="Client name">
                    </div>
                </form>
                <!-- form end -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btnSaveClientModal" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>