<div class="modal fade" id="baseModal" tabindex="-1" role="dialog" aria-labelledby="baseModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="baseModal">Add New Job</h4>
            </div>
            <div class="modal-body">
                <!-- form start -->
                <form id="frmJob" method="post" action="{{url('job')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Client</label>
                        <div class="input-group">
                            <select class="form-control ddClient" name="client_id" id="ddClient1">
                                <option value="">Select a client</option>
                            </select>
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#clientModal">New Client</button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Project</label>
                        <div class="input-group">
                            <select class="form-control ddProject" name="project_id" id="ddProject1">
                                <option value="">Select a Project</option>
                            </select>
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#projectModal">New Project</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Job label</label>
                        <input name="name" type="text" class="form-control" placeholder="Job name">
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Start date</label>
                            <div class='input-group date datepicker'>
                                <input name="start_date" type='date' class="form-control" placeholder="Start Date">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>End date</label>
                            <div class='input-group date datepicker'>
                                <input name="end_date" type='date' class="form-control" placeholder="End Date">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Team</label>
                        <select name="team_id" class="form-control ddTeam">
                            <option value="">Select a team member</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Job Status</label>
                        <div>
                            <input type="hidden" name="status" id="domJobStatus" value="In Progress" >
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary btnJobStatus active">In Progress</button>
                                <button type="button" class="btn btn-default btnJobStatus">Complete</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- form end -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btnSaveBaseModal" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

@include('modals.client')
@include('modals.project')