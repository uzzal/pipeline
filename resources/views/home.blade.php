@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="">
                <div class="calendar-bar">
                    <div class="panel-heading">
                        <div class="col-sm-2">
                            <div class="row">
                                <select class="form-control" id="ddMembers">
                                    {!! $members_options !!}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="col-sm-12">
                                @php
                                    $day_count=0;
                                @endphp
                                @foreach($days as $d)
                                    @php
                                        $day_count++;
                                    @endphp
                                    <a class="cal-box pp-job-date {{ ($day_count>4)?($day_count>8)?'hidden-md':'hidden-sm':'' }}"
                                       href="#">{{$d}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="">
                    <div class="overlay-body">
                        <div class="col-sm-10 col-sm-offset-2">
                            @include('sub.overlay')
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="clientList" class="">
                        @include('sub.job-list', ['rows'=>$rows])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
