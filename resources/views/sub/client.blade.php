<div class="panel-heading fontBld">
    {{$r->name}} <span class="badge">Client</span></div>
<div class="panel-body">
    @foreach($r->projects as $p)
        <div class="pp-project">
            <div class="pad">{{$p->name}} <span class="badge">Project</span>
            @if ($p->project_type === 'Internal')
                <span class="pipeline-project-type-Internal">({{$p->project_type}})</span>
            @else
                <span class="pipeline-project-type-Default">({{$p->project_type}})</span>
            @endif
            </div>
            @foreach($p->jobs as $j)
                <div class="pp-jobs overlay-body">
                    <div class="col-sm-2">
                        <div class="job-name">{{$j->name}} <span class="badge">Job</span></div></div>
                    <div class="col-sm-10">
                        <div class="col-sm-12 cal-overlay-wrap">
                            {!! get_plot_bar($j->start_date, $j->end_date, get_days('Y-m-d')) !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endforeach
        </div>
    @endforeach
</div>