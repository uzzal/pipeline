<?php
/**
 * @author Mahabubul Hasan <codehasan@gmail.com>
 * Date: 10/30/2017
 * Time: 5:48 PM
 */

namespace App\Http\Controllers;


use App\Repositories\Repository;
use Illuminate\Http\Request;

abstract class AppController extends Controller
{
    /**
     * @var Repository
     */
    protected $_repository;
    public function __construct(Repository $repo)
    {
        $this->_repository = $repo;
    }

    /**
     * @param Request $req
     * @return array
     */
    public function store(Request $req){
        $validator = $this->_repository->validator($req->all());
        if($validator->fails()){
            return ['status'=>'fail', 'errors' => $validator->errors()];
        }
        $data = $this->_repository->insert($req->all());
        return ['status'=>'success', 'data'=>$data];
    }
}