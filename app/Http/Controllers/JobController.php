<?php
/**
 * @author Mahabubul Hasan <codehasan@gmail.com>
 * Date: 10/30/2017
 * Time: 6:00 PM
 */

namespace App\Http\Controllers;


use App\Repositories\JobRepository;
use Illuminate\Http\Request;

class JobController extends AppController
{
    public function __construct(JobRepository $repo)
    {
        parent::__construct($repo);
    }

    public function store(Request $req)
    {
        return parent::store($req);
    }
}