<?php
/**
 * @author Mahabubul Hasan <codehasan@gmail.com>
 * Date: 10/30/2017
 * Time: 5:44 PM
 */

namespace App\Http\Controllers;


use App\Repositories\ProjectRepository;
use Illuminate\Http\Request;

class ProjectController extends AppController
{
    public function __construct(ProjectRepository $repo)
    {
        parent::__construct($repo);
    }

    public function store(Request $req)
    {
        $resp = parent::store($req);
        $client_id = $resp['data']->client_id;
        $opt = '<option value="">Select a Project</option>';
        return $opt.$this->_repository
                ->getRows()
                ->where('client_id', $client_id)
                ->asOptions();
    }

    public function optionList($client_id, $id=0){
        $opt = '<option value="">Select a Project</option>';
        return $opt.$this->_repository
                ->getRows()
                ->where('client_id', $client_id)
                ->asOptions($id);
    }


}