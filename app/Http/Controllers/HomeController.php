<?php

namespace App\Http\Controllers;

use App\Services\BundlingService;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(BundlingService $service)
    {
        return view('home', [
            'rows' => $service->clients(),
            'members_options' => $service->members(),
            'days' => $service->getDays()
        ]);
    }

    public function jobList($team_id, BundlingService $service){
        $rows = $service->clients($team_id);
        return view('sub.job-list', ['rows'=>$rows]);
    }
}
