<?php
/**
 * @author Mahabubul Hasan <codehasan@gmail.com>
 * Date: 10/30/2017
 * Time: 5:10 PM
 */

namespace App\Http\Controllers;


use App\Repositories\ClientRepository;
use Illuminate\Http\Request;

class ClientController extends AppController
{
    public function __construct(ClientRepository $repo)
    {
        parent::__construct($repo);
    }

    public function store(Request $req)
    {
        parent::store($req);
        $opt = '<option value="">Select a Client</option>';
        return $opt.$this->_repository->asOptions();
    }

    public function optionList($id=0){
        $opt = '<option value="">Select a Client</option>';
        return $opt.$this->_repository->asOptions($id);
    }

    public function show($id){
        $row = $this->_repository->getRow($id);
        return view('sub.client', ['r'=>$row]);
    }

}