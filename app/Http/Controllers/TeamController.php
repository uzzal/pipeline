<?php
/**
 * @author Mahabubul Hasan <codehasan@gmail.com>
 * Date: 11/1/2017
 * Time: 3:00 PM
 */

namespace App\Http\Controllers;

use App\Repositories\TeamRepository;

class TeamController extends AppController
{
    public function __construct(TeamRepository $repo)
    {
        parent::__construct($repo);
    }

    public function optionList($id=0){
        $opt = '<option value="">Select a Team</option>';
        return $opt.$this->_repository->asOptions($id);;
    }
}