<?php
/**
 * @author Mahabubul Hasan <codehasan@gmail.com>
 * Date: 10/30/2017
 * Time: 4:55 PM
 */

namespace App\Repositories;


use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ProjectRepository extends AbstractRepository
{
    public function __construct(Project $model)
    {
        parent::__construct($model);
    }

    public function validator(array $data, $isUpdate = false)
    {
        $this->_setValidationRule([
            'client_id'=>'required'
            ,'name'=>'required | max:200'
        ]);
        return parent::validator($data, $isUpdate);
    }

    public function insert(array $data)
    {
        return parent::insert(array_merge($data, ['created_by'=>Auth::id()]));
    }
}