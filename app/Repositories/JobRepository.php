<?php
/**
 * @author Mahabubul Hasan <codehasan@gmail.com>
 * Date: 10/30/2017
 * Time: 5:56 PM
 */

namespace App\Repositories;


use App\Models\Job;
use Auth;

class JobRepository extends AbstractRepository
{
    public function __construct(Job $model)
    {
        parent::__construct($model);
    }

    public function validator(array $data, $isUpdate = false)
    {
        $this->_setValidationRule([
            'client_id'=>'required'
            ,'project_id'=>'required'
            ,'name'=>'required'
            ,'start_date'=>'required'
            ,'end_date'=>'required'
            ,'team_id'=>'required'
            ,'status'=>'required'
        ]);
        return parent::validator($data, $isUpdate);
    }

    public function insert(array $data)
    {
        return parent::insert(array_merge($data, ['created_by'=>Auth::id()]));
    }


}