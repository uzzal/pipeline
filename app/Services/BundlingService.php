<?php
/**
 * @author Mahabubul Hasan <codehasan@gmail.com>
 * Date: 11/2/2017
 * Time: 1:04 PM
 */

namespace App\Services;

use App\Models\Client;
use App\Repositories\TeamRepository;
use App\User;
use Carbon\Carbon;
use DB;

class BundlingService
{
    private function _getStartEndDates(){
        $dates = self::getDays('Y-m-d');
        $len = count($dates);
        return [
            'start_date'=>$dates[0],
            'end_date'=>$dates[$len-1]
        ];
    }
    public function clients($user_id=0){
        if($user_id){
            $start_end_dates = $this->_getStartEndDates();

            $row = DB::table('clients as c')->join('jobs as j', 'j.client_id', 'c.client_id')
                ->selectRaw('distinct c.client_id')
                ->where('j.team_id', $user_id)
                ->where(function($q) use ($start_end_dates){
                    $q->whereBetween('j.start_date', $start_end_dates)
                        ->orWhereBetween('j.end_date', $start_end_dates);
                })->get();

            $client_ids = array_map(function($n){
                return $n->client_id;
            }, $row->toArray());

            if($client_ids){
                return Client::whereIn('client_id', $client_ids)->with('projects')->get();
            }else{
                return [];
            }
        }
        return Client::with('projects')->get();
    }

    public function members(){
        $repo = new TeamRepository(new User());
        return '<option value="">Select Member</option>'.$repo->asOptions();
    }

    public static function getDays($format='D d/m'){
        $today = Carbon::now()->format('D');
        $which_monday = ($today=='Mon')?'monday':'last monday';

        $monday = new Carbon($which_monday);
        $days = [];
        $day_count=0;
        do{
            if(!in_array($monday->format('D'), ['Sat', 'Sun'])){
                $days[] = $monday->format($format);
                $day_count++;
            }
            $monday->addDay();
        }while($day_count < 10);

        return $days;
    }

}