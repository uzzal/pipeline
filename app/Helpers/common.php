<?php
function get_days($format='Y-m-d'){
    return \App\Services\BundlingService::getDays($format);
}

function get_days_range($start, $end){
    $carbon = new \Carbon\Carbon($start);
    $dates = [];
    while(true){
        $dates[] = $t = $carbon->format('Y-m-d');
        if(strtotime($end)<=strtotime($t)){
            break;
        }
        $carbon->addDay();
    };
    return $dates;
}

function get_plot_bar($start, $end, $days){
    $dates = get_days_range($start, $end);
    $str = '';
    $c=0;
    foreach($days as $d){
        $plot='';
        if(in_array($d, $dates)){
            $plot = 'plot-date';
        }
        $c++;
        if($c>=8){
            $str .= '<div class="cal-box hidden-md '.$plot.'"></div> ';
        }else if($c>=4){
            $str .= '<div class="cal-box hidden-sm '.$plot.'"></div> ';
        }else{
            $str .= '<div class="cal-box '.$plot.'"></div> ';
        }
    }
    return $str;
}