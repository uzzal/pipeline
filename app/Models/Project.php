<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    protected $primaryKey = 'project_id';

    protected $fillable = [
        'client_id', 'name', 'created_by', 'is_active','project_type'
    ];

    public function jobs(){
        return $this->hasMany(Job::class, 'project_id', 'project_id');
    }
}
