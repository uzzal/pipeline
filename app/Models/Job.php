<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';

    protected $primaryKey = 'job_id';

    protected $fillable = [
        'client_id', 'project_id', 'team_id', 'created_by', 'name', 'start_date', 'end_date', 'status', 'is_active'
    ];


}
