<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';

    protected $primaryKey = 'client_id';

    protected $fillable = [
        'name', 'created_by', 'is_active'
    ];

    public function projects(){
        return $this->hasMany(Project::class, 'client_id', 'client_id')->with('jobs');
    }
}
